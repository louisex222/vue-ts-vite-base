export interface InterLiveGame {
    desk:string,
    thirdPartyId :string,
    sort: number,
    name: string,
    category: number[],
    active:boolean,
    clubType: string,
    serverId: string
}
export interface InterClubList
{
    id: string,
    gameType: number,
    thirdPartyId: string,
    code: string,
    active: boolean,
}

export interface InterLiveCategory
{
    id: number,
    gameType: number,
    thirdPartyId: string,
    clubType: string,
    name: string,
    active: boolean,
    localizationCode: string
    imageName: string
    imagePath: string,
    sort: number,
}

