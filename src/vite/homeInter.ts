export interface InterCategory {
    id: number
}
export interface InterCreateImage {
    (imagePath: any, id: string): string
}

export interface InterSlotGame {
    name: string,
    id: number,
    imagePath: any,
}

export interface InterMixList {
    clubId: number
    categoryIdList: number[]
    sort: number
}

export interface InterList {
    clubId: number
    id: number
}



