import { apiGetGameClassListCommon, apiGetGameListCommon, apiGetTimeRange } from '@/service/type/config'
import { apiGetGameRecords } from '@/service/report/index'
import { useStore } from 'vuex';
import { useI18n } from 'vue-i18n';
import { useRouter } from 'vue-router';
interface Form {
    date: string
    dateStart: string
    dateEnd: string
    class: number
    club: string
    game: string
    total: number
}
interface interDataList {
    text: string,
    value: string
    click: boolean
}
interface IReportData {
    thirdParty_Id: string,
    maHao: string,
    datetime: string,
}
export default function useReport() {
    const { locale } = useI18n()
    const store = useStore()
    const router = useRouter()
    const dateMin: Ref<string> = ref('')
    const dateMax: Ref<string> = ref('')
    const reportVisible = computed(() => store.getters.reportVisible)
    store.commit('changeRepostVisible', localStorage.getItem('visible') === 'true' ? true : false)
    const form: Ref<Form> = ref({
        date: '',
        dateStart: '',
        dateEnd: '',
        class: 0,
        club: 'ALL',
        game: 'ALL',
        total: 0,
    })
    const timeData: Ref<any> = ref({})
    /**
     * fcGetTimeRange 取得時間區間
     * @return void
     */
    const fcGetTimeRange = async (): Promise<void> => {
        const result: any = await apiGetTimeRange()
        if (result.status) {
            timeData.value = result.result[0]
        }
    }
    fcGetTimeRange()
    const dateList: Ref<interDataList[]> = ref([
        { text: '當天', value: 'today', click: true },
        { text: '本週', value: 'thisWeek', click: true },
        { text: '上週', value: 'lastWeek', click: true },
        { text: '本月', value: 'thisMonth', click: true },
        { text: '上月', value: 'lastMonth', click: true }
    ])
    const tableData: Ref<any[]> = ref(localStorage.getItem('reportData') ? JSON.parse(localStorage.getItem('reportData') as string) : [])
    const currentPage: Ref<number> = ref(1)
    const gametypeList = computed(() => {
        const list = [{ code: '', text: 'all_report', value: 0 },
        { code: 'Live', text: 'liveGame_report', value: 1 },
        { code: 'Slot', text: 'slotGame_report', value: 3 },
        { code: 'Table', text: 'tableGame_report', value: 5 },
        { code: 'Sport', text: 'sportGame_report', value: 4 },
        { code: 'Lottery', text: 'lotteryGame_report', value: 6 },
        { code: 'Animal', text: 'animal_game_report', value: 7 },
        { code: 'Esport', text: 'esport_game_report', value: 8 },]
        return list
    })
    const clubList: Ref<any[]> = ref([])
    const gameList: Ref<any[]> = ref([])
    /**
     * fcChangeTime 改變時間
     * @param time 時間
     * @return void
     */
    const fcChangeTime = (time: interDataList): void => {
        const result = time.value;
        switch (result) {
            case 'today':
                form.value.dateStart = timeData.value.today_Begin
                form.value.dateEnd = timeData.value.today_End

                break;
            case 'thisWeek':
                form.value.dateStart = timeData.value.week_Begin
                form.value.dateEnd = timeData.value.week_End

                break;
            case 'lastWeek':
                form.value.dateStart = timeData.value.preWeek_Begin
                form.value.dateEnd = timeData.value.preWeek_End

                break;
            case 'thisMonth':
                form.value.dateStart = timeData.value.mon_Begin
                form.value.dateEnd = timeData.value.mon_End
                break;
            case 'lastMonth':
                form.value.dateStart = timeData.value.preMon_Begin
                form.value.dateEnd = timeData.value.preMon_End
                break;
        }
        form.value.date = result
        dateMin.value = form.value.dateStart
        dateMax.value = form.value.dateEnd
    }
    /**
     * fcChangeClass 改變遊戲種類
     * @param value 遊戲種類
     * @return void
     */
    const fcChangeClass = async (value: number = 0): Promise<void> => {
        form.value.class = value
        form.value.club = 'ALL'
        form.value.game = 'ALL'
        const param = {
            gameClass: form.value.class
        }
        const result: any = await apiGetGameClassListCommon(param)
        if (result.status) {
            clubList.value = result.result
        }
    }
    fcChangeClass(0)
    /**
     * fcChangeClub 改變遊戲類別
     * @param value 遊戲類別
     * @return void
     */
    const fcChangeClub = async (value: string = 'ALL'): Promise<void> => {
        form.value.club = value
        form.value.game = 'ALL'
        const param = {
            gameType: form.value.class,
            thirdParty_Id: value,
        }
        const result: any = await apiGetGameListCommon(param)
        if (result.status) {
            gameList.value = result.result
        }
    }
    fcChangeClub('ALL')
    /**
     * fcChangeGame 改變遊戲名稱
     * @param value 遊戲名稱
     * @return void
     */
    const fcChangeGame = (value: string = 'ALL'): void => {
        form.value.game = value
    }
    fcChangeGame('ALL')

    /**
     * fcSubmitForm 改變遊戲名稱
     * @param value 遊戲名稱
     * @return void
     */
    const fcSubmitForm = async (): Promise<void> => {
        store.commit('changeRepostVisible', true)
        const param = {
            language: locale.value.toLowerCase(),
            dateStart: form.value.dateStart,
            dateEnd: form.value.dateEnd,
            game_Class: form.value.class.toString(),
            thirdParty_Id: form.value.club,
            gType: form.value.game,
            row_Set: form.value.total,
            reportType: form.value.date === 'today' ? 'n' : 'h',
            wType: 'ALL'
        }
        const result: any = await apiGetGameRecords(param)
        if (result.status) {
            const { dataInfo } = result.result
            tableData.value = dataInfo
            localStorage.setItem('reportData', JSON.stringify(dataInfo))
            localStorage.setItem('visible', 'true')
        }
    }

    const fcChangeVisible = (): void => {
        store.commit('changeRepostVisible', false)
        localStorage.setItem('reportData', JSON.stringify([]))
        localStorage.setItem('visible', 'false')
    }

    const fcChangePage = (page: number): void => {
        currentPage.value = page
    }

    const fcOpenBox = (row: IReportData): void => {

        const reportData = {
            thirdParty_Id: row.thirdParty_Id,
            summary_id: row.maHao.split(',')[3],
            reportTime: row.datetime,
        }
        router.push({ name: 'reportHistory', query: { param: JSON.stringify(reportData) } })
    }
    return {
        form,
        timeData,
        dateList,
        tableData,
        currentPage,
        clubList,
        gametypeList,
        gameList,
        dateMin,
        dateMax,
        reportVisible,
        fcChangeTime,
        fcSubmitForm,
        fcChangeVisible,
        fcChangePage,
        fcChangeClass,
        fcChangeClub,
        fcChangeGame,
        fcOpenBox,
    }
}