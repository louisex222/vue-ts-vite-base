
import {Ref} from "vue";
import {InterClubList, InterLiveCategory, InterLiveGame} from "@/vite/game";
import {apiGetGameTableApi, apiGetLiveCategories} from "@/service/game/detail";
import {apiGetClubList} from "@/service/type/config";

export default function useLive () {
    const liveCategoryList: Ref<InterLiveCategory[]> = ref([])
    const categoryType : Ref<string> = ref('')
    const clubList: Ref<InterClubList[]> = ref([]);
    const clubType: Ref<string> = ref('')
    const tableGameList: Ref<InterLiveGame[]> = ref([]);

    const fcGetLiveCategories = async():Promise<void>=>{
        const param ={
            gameType: 1
        }
       const res = await apiGetLiveCategories(param)
        try{
            if(res){
                liveCategoryList.value = res
            }
        }catch(error){
            console.error('Error in fetching data:', error);
        }
    }
    const fcGetClubList = async()=>{
        const res = await apiGetClubList()
        try{
        if(res){
            clubList.value = res.filter((club:any)=>{
                const list :string[] = ['RCG','WM']
                return club.gameType === 1 && list.includes(club.thirdPartyId)
            })
        }
        }catch(error){
            console.error('Error in fetching data:', error);
        }
    }

    const fcGetTableList = async() => {
        const param = {
            gameType: 1
        }
        try{
        const res = await apiGetGameTableApi(param)
            if(res){
                tableGameList.value = res
            }
        }catch(error){
            console.error('Error in fetching data:', error);
        }
    }

    const fcChangeCategory = (club:string )=>{
        categoryType.value = club
        clubType.value = ''
    }

    const fcChangeClub = (thirdPartyId:string)=>{
        clubType.value = thirdPartyId
    }

    onMounted(async () => {
        try {
            await fcGetTableList()
            await fcGetLiveCategories()
            await fcGetClubList()
        } catch (error) {
            console.error('Error in fetching data:', error);
        }
    })
    return {
        liveCategoryList,
        clubList,
        categoryType,
        clubType,
        fcChangeClub,
        fcChangeCategory,
        tableGameList
    }
}
