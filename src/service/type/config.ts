import index from '../index-nojwt'

// 獲取遊戲分類
export const apiGetGameTypeList: any = (): Promise<any> => {
    return index({
        url: '/webCache/GetGameTypeList',
        method: 'get',
        data: {}
    })
}
// 獲取電子分類
export const apiGetCategoryList: any = (): Promise<any> => {
    return index({
        url: '/webCache/GetCategoryList',
        method: 'get',
        data: {}
    })
}
/**
 * 取得館別分類
 */
export const apiGetClubList: any = (): Promise<any> => {
    return index({
        url: '/webCache/GetClubList2',
        method: 'get',
        data: {}
    })
}
/**
 * 取得Club分類
 */
export const apiGetGameClassListCommon: any = (data: number): Promise<any> => {
    return index({
        url: '/api/Game/ClassListCommon',
        method: 'post',
        data
    })
}
/**
 * 取得Game分類
 */
export const apiGetGameListCommon: any = (data: string): Promise<any> => {
    return index({
        url: '/api/Game/ListCommon',
        method: 'post',
        data
    })
}
/**
 * 取得時間設定
 */
export const apiGetTimeRange: any = (): Promise<any> => {
    return index({
        url: '/api/Utils/TimeRange',
        method: 'post',
        data: {}
    })
}


