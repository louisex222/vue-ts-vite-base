import index from '../index-nojwt'

export const rcgandroyalHotGameListApi = (): Promise<any> => {
    return index({
        url: '/WebCache/GetHomePageSetting',
        method: 'get',
        data: {}
    })
}

export const getSlotGameListApi = (club: string): Promise<any> => {
    return index({
        url: `/webCache/GetSlotGame${club}List`,
        method: 'get',
        data: {}
    })
}
export const apiGetLiveCategories =(param:any) :Promise<any>=>{
    return index({
        url:'/webCache/GetAllClubTypeList',
        method:'post',
        data:param
    })
}
export const apiGetGameTableApi = (param : any) : Promise<any> =>{
    return index({
        url: '/webCache/GetAllTableList',
        method:'post',
        data:param
    })
}
export const apiLiveGameRoad = (param:any) :Promise<any>=>{
    return index({
        url:`/rm/RCG/${param.gameType}`,
        method:'get',
        data:param
    })
}
