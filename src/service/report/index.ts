import index from '../index'

// 獲取第一層遊戲報表資料
interface IGetGameReport {
    language: string
    dateStart: string
    dateEnd: string,
    game_Class: string,
    thirdParty_Id: string,
    gType: string,
    row_Set: number
    reportType: string
    wType: string
}
interface IGetGameHistory {
    summary_id: string,
    reportTime: string,
    web_Id: string,
    pageIndex: string,
    pageSize: string,
}
export const apiGetGameRecords: any = (data: IGetGameReport): Promise<any> => {
    return index({
        url: '/api/Report/GameRecord2',
        method: 'post',
        data: data
    })
}

export const apiGetGameHistory: any = (club: string, data: IGetGameHistory,): Promise<any> => {
    return index({
        url: `/api/Report/History/${club}`,
        method: 'post',
        data: data
    })
}